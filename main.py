import numberGenerator
import outputModule
import yaml

PARAM_FILE = "params/parameters.yaml"
INPUT_FILE = "inputs/inputs.txt"
OUTPUT_FILE = "outputs/outputs.txt"

def getParameters() -> dict:
    with open(PARAM_FILE) as f:
        return yaml.safe_load(f)

def saveInput(input):
    with open(INPUT_FILE,"w") as f:
        for i in input:
            f.write(str(i)+"\n")

def saveOutput(output):
    with open(OUTPUT_FILE,"w") as f:
        f.write(str(output))

if __name__ == '__main__':
    print("Getting parameters from the 'parameters.yaml' file...")
    params = getParameters()
    min = params["min_value"]
    max = params["max_value"]
    numbers_to_generate = params["numbers_to_generate"]

    if (params["seed"] != None):
        numberGenerator.setSeed(params["seed"])
    print(f"Generating {numbers_to_generate} random numbers between {min} and {max}...")
    numbers = numberGenerator.generateNumbers(numbers_to_generate,min,max)
    print("Generating output...")
    output = outputModule.computeValues(numbers,min,max)

    saveInput(numbers)
    saveOutput(output)